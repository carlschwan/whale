// SPDX-FileCopyrightText: 2024 Claudio Cambra <claudio.cambra@kde.org>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "navigationstack.h"

NavigationStack::NavigationStack()
    : QObject()
{
    updateCurrentUrlModel();
}

void NavigationStack::updateCurrentUrlModel()
{
    m_currentUrlModel.setUrl(currentUrl());
}

QUrl NavigationStack::currentUrl() const
{
    return m_currentUrl;
}

void NavigationStack::setCurrentUrl(const QUrl &url)
{
    m_currentUrl = url;
    Q_EMIT currentUrlChanged();
    updateCurrentUrlModel();
}

const PathModel *NavigationStack::currentUrlModel() const
{
    return &m_currentUrlModel;
}

bool NavigationStack::backwardsAvailable() const
{
    return !m_backwardStack.empty();
}

bool NavigationStack::forwardsAvailable() const
{
    return !m_forwardStack.empty();
}

void NavigationStack::pushUrl(const QUrl &url)
{
    if (url == currentUrl()) {
        return;
    }

    const auto prePushBackwardsAvailable = backwardsAvailable();
    const auto prePushForwardsAvailable = forwardsAvailable();

    if (const auto current = currentUrl(); current.isValid()) {
        m_backwardStack.push(current);
    }
    setCurrentUrl(url);
    while (!m_forwardStack.empty()) {
        m_forwardStack.pop();
    }

    if (prePushBackwardsAvailable != backwardsAvailable()) {
        Q_EMIT backwardsAvailableChanged();
    }
    if (prePushForwardsAvailable != forwardsAvailable()) {
        Q_EMIT forwardsAvailableChanged();
    }
}

void NavigationStack::goBackwards()
{
    const auto prePopBackwardsAvailable = backwardsAvailable();
    const auto prePopForwardsAvailable = forwardsAvailable();

    m_forwardStack.push(currentUrl());
    setCurrentUrl(m_backwardStack.top());
    m_backwardStack.pop();

    if (prePopBackwardsAvailable != backwardsAvailable()) {
        Q_EMIT backwardsAvailableChanged();
    }
    if (prePopForwardsAvailable != forwardsAvailable()) {
        Q_EMIT forwardsAvailableChanged();
    }
}

void NavigationStack::goForwards()
{
    const auto prevBackwardsAvailable = backwardsAvailable();
    const auto prevForwardsAvailable = forwardsAvailable();

    m_backwardStack.push(currentUrl());
    setCurrentUrl(m_forwardStack.top());
    m_forwardStack.pop();

    if (prevBackwardsAvailable != backwardsAvailable()) {
        Q_EMIT backwardsAvailableChanged();
    }
    if (prevForwardsAvailable != forwardsAvailable()) {
        Q_EMIT forwardsAvailableChanged();
    }
}

