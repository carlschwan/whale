// SPDX-FileCopyrightText: 2024 Claudio Cambra <claudio.cambra@kde.org>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QAbstractListModel>
#include <qqmlregistration.h>
#include <QUrl>

class PathModel : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT

public:
    enum Roles { UrlRole = Qt::UserRole + 1 };
    Q_ENUM(Roles);

    void setUrl(const QUrl &url);

    int rowCount(const QModelIndex &parent = {}) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE QUrl followingPathComponent(int row) const;

private:
    struct PathComponent {
        QString name;
        QUrl url;
    };
    std::vector<PathComponent> m_components;
};

