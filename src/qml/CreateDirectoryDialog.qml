// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as Controls

import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard
import org.kde.whale

FormCard.FormCardDialog {
    id: root

    property string parentPath: ""

    title: i18nc("@title:dialog", "Create new folder")
    standardButtons: Controls.Dialog.Cancel | Controls.Dialog.Ok

    onAccepted: {
        DirModelUtils.mkdir(parentPath + "/" + nameField.text)
        root.close()
    }

    Component.onCompleted: {
        root.standardButton(Controls.Dialog.Ok).enabled = Qt.binding(() => {
            return nameField.text.length > 0 && !nameField.text.includes('/');
        });
    }

    onRejected: {
        nameField.clear()
        root.close()
    }

    FormCard.AbstractFormDelegate {
        id: locationLabel
        text: i18nc("@info", "Create new folder in '%1':", root.parentPath.replace("file://", ""))
        background: null
        bottomPadding: 0
        contentItem: Controls.Label {
            text: locationLabel.text
        }
    }

    FormCard.FormTextFieldDelegate {
        id: nameField
        label: i18nc("@label:textfield", "Folder name:")
        text: i18nc("default value when creating a new folder", "New Folder")
    }
}
