/*
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as QQC2
import org.kde.kirigami as Kirigami
import org.kde.whale

Kirigami.ApplicationWindow {
    id: root
    globalDrawer: PlacesGlobalDrawer {
        modal: !root.wideScreen
        onModalChanged: drawerOpen = !modal
        contentItem.implicitWidth: Kirigami.Units.gridUnit * 10
        onPlaceOpenRequested: {
            filePicker.folder = place;

        }
    }
    pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.None

    header: QQC2.ToolBar {
        implicitHeight: Kirigami.Units.gridUnit * 1.8 + Kirigami.Units.smallSpacing * 2

        contentItem: RowLayout {
            Item {
                id: leftHandleAnchor
                visible: (typeof applicationWindow() !== "undefined" && applicationWindow().globalDrawer && applicationWindow().globalDrawer.enabled && applicationWindow().globalDrawer.handleVisible &&
                applicationWindow().globalDrawer.handle.handleAnchor == leftHandleAnchor) &&
                (globalToolBar.canContainHandles || (breadcrumbLoader.pageRow.firstVisibleItem &&
                breadcrumbLoader.pageRow.firstVisibleItem.globalToolBarStyle == Kirigami.ApplicationHeaderStyle.ToolBar))
                Layout.preferredWidth: height
            }

            Kirigami.ActionToolBar {
                actions: [
                    Kirigami.Action {
                        text: i18nc("@action:intoolbar", "Back")
                        icon.name: "arrow-left"
                        enabled: NavigationStack.backwardsAvailable
                        onTriggered: NavigationStack.goBackwards()
                    },
                    Kirigami.Action {
                        text: i18nc("@action:intoolbar", "Forwards")
                        icon.name: "arrow-right"
                        enabled: NavigationStack.forwardsAvailable
                        onTriggered: NavigationStack.goForwards()
                    },
                    Kirigami.Action {
                        icon.name: "folder"
                        text: i18n("Create folder")
                        visible: !root.selectExisting

                        onTriggered: filePicker.createDirectoryDialog.open()
                    },
                    Kirigami.Action {
                        id: filterAction
                        icon.name: "view-filter"
                        checkable: true
                        text: i18n("Filter filetype")
                        checked: true
                    }
                ]
            }
        }
    }
    pageStack.initialPage: MillerColumnsPage {
        id: filePicker

    }

    Component.onCompleted: ShiftHandler.target = root;
}
