﻿// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2018 Camilo Higuita <milo.h@aol.com>
// SPDX-FileCopyrightText: 2024 Claudio Cambra <claudio.cambra@kde.org>
//
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as Controls
import org.kde.kirigami as Kirigami

import org.kde.whale

Kirigami.Page {
    id: root

    function addOrRemoveUrl(url) {
        const index = root.fileUrls.indexOf(url)
        if (index > -1) {
            // remove element
            root.fileUrls.splice(index, 1)
        } else {
            root.fileUrls.push(url)
        }
        root.fileUrlsChanged()
    }

    signal accepted(var urls)

    property bool selectMultiple
    property bool selectExisting
    property var nameFilters: []
    property var mimeTypeFilters: []
    property alias folder: root.startFolderModel.url
    property string currentFile
    property string acceptLabel
    property bool selectFolder
    property bool shifty: false
    property int shiftyAnchor: -1

    // result
    property var fileUrls: []

    readonly property CreateDirectoryDialog createDirectoryDialog: CreateDirectoryDialog {
        id: createDirectoryDialog
        parentPath: startFolderModel.url
    }

    readonly property FolderModel startFolderModel: FolderModel {
        //showDotFiles: false
        filterMimeTypes: root.mimeTypeFilters
        //onLastErrorChanged: errorMessage.visible = true
        //onFolderChanged: errorMessage.visible = false
        Component.onCompleted: url = NavigationStack.currentUrl
    }

    onCurrentFileChanged: {
        if (root.currentFile) {
            // Switch into directory of preselected file
            fileNameField.text = DirModelUtils.fileNameOfUrl(root.currentFile)
            startFolderModel.folder = DirModelUtils.directoryOfUrl(root.currentFile)
        }
    }

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    Kirigami.Theme.colorSet: Kirigami.Theme.View
    Kirigami.Theme.inherit: false

    header: ColumnLayout {
        spacing: 0
        Controls.ToolBar {
            Layout.fillWidth: true
            Row {
                Repeater {
                    id: pathControlRepeater
                    model: NavigationStack.currentUrlModel

                    Controls.ToolButton {
                        icon.name: "arrow-right"
                        text: model.display
                        onClicked: {
                        }
                    }
                }
            }
        }
        Kirigami.InlineMessage {
            id: errorMessage
            Layout.fillWidth: true
            type: Kirigami.MessageType.Error
            text: startFolderModel.errorString
            showCloseButton: true
        }
    }

    actions: [
        Kirigami.Action {
            visible: (root.selectMultiple || root.selectFolder) && root.selectExisting
            text: root.acceptLabel ? root.acceptLabel : i18n("Select")
            icon.name: "dialog-ok"

            onTriggered: {
                if (root.selectFolder) {
                    root.accepted([startFolderModel.folder])
                } else {
                    root.accepted(root.fileUrls)
                }
            }
        }
    ]
}

