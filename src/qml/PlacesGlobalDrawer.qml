// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as Controls
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.delegates as Delegates
import org.kde.whale

/**
 * The PlacesGlobalDrawer type provides a GlobalDrawer containing common places on the file system
 */
Kirigami.OverlayDrawer {
    id: root

    signal placeOpenRequested(url place)

    handleClosedIcon.source: null
    handleOpenIcon.source: null
    width: Math.min(applicationWindow().width * 0.8, Kirigami.Units.gridUnit * 13)

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    contentItem: Controls.ScrollView {
        ListView {
            spacing: 0
            model: FilePlacesModel {
                id: filePlacesModel
            }

            section {
                property: "group"
                delegate: Kirigami.Heading {
                    topPadding: Kirigami.Units.smallSpacing
                    leftPadding: Kirigami.Units.largeSpacing
                    level: 6
                    text: section
                    opacity: 0.7
                }
            }

            delegate: Delegates.RoundedItemDelegate {
                required property string name
                required property string iconName
                required property bool hidden
                required property url url

                visible: !hidden
                width: ListView.view.width
                contentItem: RowLayout {
                    spacing: Kirigami.Units.smallSpacing

                    Kirigami.Icon {
                        Layout.preferredWidth: Kirigami.Units.iconSizes.small
                        Layout.preferredHeight: Kirigami.Units.iconSizes.small
                        source: iconName
                    }
                    Controls.Label {
                        Layout.fillWidth: true
                        text: name
                        elide: Text.ElideRight
                    }
                }
                onClicked: {
                    root.placeOpenRequested(url);
                }
            }
        }
    }
}
