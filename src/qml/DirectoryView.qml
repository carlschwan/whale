﻿// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2018 Camilo Higuita <milo.h@aol.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as Controls
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.delegates as Delegates

import org.kde.whale

ListView {
    id: _listView

    signal triggerUrl(string url, bool isDir)

    required property FolderModel folderModel
    property bool triggerUrlOnSingleClick: false

    model: folderModel
    keyNavigationEnabled: true
    keyNavigationWraps: true
    currentIndex: -1
    clip: true

    onCurrentIndexChanged: {
        if (shifty) {
            folderModel.setRangeSelected(shiftyAnchor, currentIndex)
            return
        }
        folderModel.clearSelection()
        folderModel.toggleSelected(currentIndex)
    }

    Loader {
        asynchronous: true
        anchors.fill: parent
        active: !Kirigami.Settings.hasTransientTouchInput && !Kirigami.Settings.isMobile

        sourceComponent: MouseArea {
            id: _mouseArea

            function updateLassoStartGeometry() {
                selectLayer.x = mouseX;
                selectLayer.y = mouseY;
                selectLayer.newX = mouseX;
                selectLayer.newY = mouseY;
                selectLayer.width = 0
                selectLayer.height = 0;
            }

            function startLasso() {
                updateLassoStartGeometry();
                selectLayer.visible = true;
            }

            preventStealing: true
            propagateComposedEvents: true
            acceptedButtons: Qt.LeftButton

            onDoubleClicked: (mouse) => {
                if (mouse.source === Qt.MouseEventNotSynthesized && !Kirigami.Settings.hasTransientTouchInput) {
                    if (ShiftHandler.shiftPressed) {
                        folderModel.pinSelection();
                    }
                    const item = _listView.itemAt(_listView.width / 2, mouseY + _listView.contentY);
                    if (item === null) {
                        folderModel.clearSelection();
                        mouse.accepted = false;
                        return;
                    }
                    updateLassoStartGeometry();
                    item.handleDoublePress(mouse.button);
                }
            }

            onPositionChanged: (mouse) => {
                if (!_mouseArea.pressed || !selectLayer.visible) {
                    mouse.accepted = false;
                    return;
                }

                if (mouseX >= selectLayer.newX) {
                    selectLayer.width = (mouseX + 9) < (_listView.x + _listView.width) ? (mouseX - selectLayer.x) : selectLayer.width;
                } else {
                    selectLayer.x = mouseX < _listView.x ? _listView.x : mouseX;
                    selectLayer.width = selectLayer.newX - selectLayer.x;
                }

                if(mouseY >= selectLayer.newY) {
                    selectLayer.height = (mouseY + 9) < (_listView.y + _listView.height) ? (mouseY - selectLayer.y) : selectLayer.height;
                    if (!_listView.atYEnd &&  mouseY > (_listView.y + _listView.height)) {
                        _listView.contentY += 9
                    }
                } else {
                    selectLayer.y = mouseY < _listView.y ? _listView.y : mouseY;
                    selectLayer.height = selectLayer.newY - selectLayer.y;

                    if (!_listView.atYBeginning && selectLayer.y === -1) {
                        _listView.contentY -= 9
                    }
                }
            }

            onPressed: (mouse) => {
                if (_mouseArea.drag.target !== null) {
                    return;
                }

                if (mouse.source === Qt.MouseEventNotSynthesized && mouse.button === Qt.LeftButton && !Kirigami.Settings.hasTransientTouchInput) {
                    if (ShiftHandler.shiftPressed) {
                        folderModel.pinSelection();
                    }
                    const item = _listView.itemAt(_listView.width / 2, mouseY + _listView.contentY)
                    if (item === null) {
                        folderModel.clearSelection();
                        mouse.accepted = false;
                        return;
                    } else if (item.mapFromItem(_mouseArea, mouseX, mouseY).x < item.implicitWidth) {
                        selectLayer.reset();
                        _mouseArea.drag.target = item;
                        item.handlePress(mouse.button);
                        return;
                    }
                    startLasso(); 
                    item.handlePress(mouse.button);
                    mouse.accepted = true;
                } else {
                    mouse.accepted = false;
                }
            }

            onPressAndHold: (mouse) => {
                if (mouse.button === Qt.LeftButton && !Kirigami.Settings.hasTransientTouchInput) {
                    const item = _listView.itemAt(_listView.width / 2, mouseY + _listView.contentY);
                    if (item !== null) {
                        selectLayer.reset();
                        _mouseArea.drag.target = item;
                        return;
                    }
                }
                mouse.accepted = true;
            }

            onReleased: (mouse) => {
                if (_mouseArea.drag.target !== null) {
                    _mouseArea.drag.target = null;
                    return;
                }

                if (mouse.button !== Qt.LeftButton || !selectLayer.visible || selectLayer.height === 0) {
                    mouse.accepted = false
                    return;
                }

                if (selectLayer.y > _listView.contentHeight) {
                    selectLayer.reset();
                    return;
                }

                let lassoIndexes = []
                const limitY =  mouse.y === selectLayer.y ?  selectLayer.y+selectLayer.height : mouse.y

                for(let y = selectLayer.y; y < limitY; y+=10) {
                    const index = _listView.indexAt(_listView.width/2,y+_listView.contentY)
                    if (!lassoIndexes.includes(index) && index>-1 && index< _listView.count) {
                        lassoIndexes.push(index)
                    }
                }

                folderModel.updateSelection(lassoIndexes, true)
                selectLayer.reset()
                mouse.accepted = true
            }
        }
    }

    RubberBand {
        id: selectLayer
        property int newX: 0
        property int newY: 0
        height: 0
        width: 0
        x: 0
        y: 0
        visible: false

        function reset() {
            selectLayer.x = 0;
            selectLayer.y = 0;
            selectLayer.newX = 0;
            selectLayer.newY = 0;
            selectLayer.visible = false;
            selectLayer.width = 0;
            selectLayer.height = 0;
        }
    }

    delegate: Delegates.RoundedItemDelegate {
        id: delegate
        required property int index
        required property string name
        required property string iconName
        required property url url
        required property bool isDir
        required property bool selected
        required property bool softSelected

        function handlePress(button) {
            if (button === Qt.LeftButton && ShiftHandler.shiftPressed) {
                folderModel.setSelected(index);
            } else if (button == Qt.LeftButton) {
                folderModel.clearSelection();
                folderModel.setSelected(index);
                if (_listView.triggerUrlOnSingleClick) {
                    if (isDir) {
                        NavigationStack.pushUrl(url);
                    }
                    _listView.triggerUrl(url, isDir);
                }
            } else if (button === Qt.RightButton) {
                if (!folderModel.isSelected(index)) {
                    folderModel.clearSelection();
                }
                folderModel.setSelected(index);
                folderModel.openContextMenu(parent)
            }
        }

        function handleDoublePress(button) {
            if (isDir) {
                NavigationStack.pushUrl(url);
            }
            _listView.triggerUrl(url, isDir);
        }

        width: ListView.view.width
        text: name
        checked: _listView.currentIndex == index
        icon.name: checked ? "emblem-checked" : iconName
        highlighted: selected || softSelected
        font.bold: softSelected
        onCheckedChanged: if (checked) folderModel.setSelected(index)
        onSelectedChanged: if (selected && !checked) _listView.currentIndex == index
        onSoftSelectedChanged: if (softSelected && !checked) _listView.currentIndex == index
        onDoubleClicked: handleDoublePress()

        TapHandler {
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onTapped: (eventPoint, button) => {
                handlePress(button);
            }
        } 
    }
}

