﻿// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2018 Camilo Higuita <milo.h@aol.com>
//
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as Controls
import org.kde.kirigami as Kirigami

import org.kde.whale

BaseViewPage {
    id: root

    contentItem: Controls.ScrollView {
        DirectoryView {
            folderModel: root.startFolderModel
            onTriggerUrl: (url, isDir) => {
                if (isDir) {
                    // Change into folder
                    root.startFolderModel.url = url;
                }
                // The delegate represents a file
                else {
                    //folderModel.runSelected()
                    //if (_listView.selectMultiple) {
                    //    // add the file to the list of accepted files
                    //    // (or remove it if it is already there)
                    //    _listView.addOrRemoveUrl(url)
                    //} else {
                    //    // If we only want to select one file,
                    //    // Write it into the output variable and close the dialog
                    //    _listView.fileUrls = [url]
                    //    _listView.accepted(_listView.fileUrls)
                    //}
                }
            }
        }
    }
}

