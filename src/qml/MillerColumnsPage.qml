// SPDX-FileCopyrightText: 2024 Claudio Cambra <claudio.cambra@kde.org>
//
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as Controls
import org.kde.kirigami as Kirigami

import org.kde.whale

BaseViewPage {
    id: root

    property var _defaultColumnWidth: 200
    property var _columnWidths: []

    Connections {
        target: NavigationStack.currentUrlModel

        function onRowsInserted(): void {
            if (_columnWidths.length < NavigationStack.currentUrlModel.rowCount()) {
                _columnWidths.push(_defaultColumnWidth);
            }
        }
    }

    Component {
        id: folderModel
        FolderModel {}
    }

    Component {
        id: folderView
        DirectoryView {}
    }

    contentItem: Controls.ScrollView {
        ListView {
            id: stackView
            spacing: 0
            orientation: Qt.Horizontal
            onCountChanged: positionViewAtEnd()

            model: NavigationStack.currentUrlModel
            delegate: RowLayout {
                id: folderContentsDelegate

                property var userSetWidth: _columnWidths[index] ?? _defaultColumnWidth
                onUserSetWidthChanged: _columnWidths[index] = userSetWidth

                width: userSetWidth
                height: ListView.view.height
                spacing: 0

                Controls.ScrollView {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    DirectoryView {
                        id: dirView
                        triggerUrlOnSingleClick: true
                        folderModel: FolderModel {
                            url: model.url
                            permanentSelection: NavigationStack.currentUrlModel.followingPathComponent(model.index)
                        }

                        Connections {
                            target: NavigationStack
                            function onCurrentUrlChanged() {
                                dirView.folderModel.permanentSelection = NavigationStack.currentUrlModel.followingPathComponent(model.index)
                            }
                        }
                    }
                }

                Kirigami.Separator {
                    id: resizer

                    Layout.preferredWidth: 1
                    Layout.fillHeight: true

                    MouseArea {
                        anchors.fill: parent
                        preventStealing: true
                        hoverEnabled: true
                        acceptedButtons: Qt.LeftButton
                        cursorShape: !Kirigami.Settings.isMobile ? Qt.SplitHCursor : null

                        property real initX: 0
                        property real initWidth: 0

                        onPressed: (mouse) => {
                            initX = mapToGlobal(mouse.x, mouse.y).x;
                            initWidth = folderContentsDelegate.userSetWidth;
                        }

                        onPositionChanged: (mouse) => {
                            if (pressed) {
                                const position = mapToGlobal(mouse.x, mouse.y)
                                const diffX = position.x - initX;
                                folderContentsDelegate.userSetWidth = initWidth + diffX;
                            }
                        }
                    }
                }
            }
        }
    }
}


