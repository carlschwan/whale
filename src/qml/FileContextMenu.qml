/*
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Qt.labs.platform

Menu {
    id: zoomMenu
    required property url itemUrl

    MenuItem {
        text: i18n("Open with %1", application)
        shortcut: StandardKey.ZoomIn
        onTriggered: zoomIn()
    }

    MenuItem {
        text: qsTr("Zoom Out")
        shortcut: StandardKey.ZoomOut
        onTriggered: zoomOut()
    }
}
