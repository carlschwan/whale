// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "dirlister.h"

#include <KIO/Job>

DirLister::DirLister(QObject *parent)
    : KDirLister(parent)
{
    connect(this, &KCoreDirLister::jobError, this, &DirLister::handleError);
}

void DirLister::handleError(KIO::Job *const job)
{
    Q_EMIT errorOccured(job->errorString());
}
