#! /bin/sh
# SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
# SPDX-License-Identifier: CC0-1.0
$XGETTEXT `find . -name \*.cpp -o -name \*.h -o -name \*.qml` -o $podir/koko.pot
