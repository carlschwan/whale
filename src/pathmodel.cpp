// SPDX-FileCopyrightText: 2024 Claudio Cambra <claudio.cambra@kde.org>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "pathmodel.h"

void PathModel::setUrl(const QUrl &url)
{
    beginResetModel();
    m_components.clear();
    static const auto slash = QStringLiteral("/");
    auto usedUrlPath = url.path();

    { // Handle the root case, i.e. the path [ / ] in the local filesystem
        auto rootUrl = QUrl(url);
        rootUrl.setPath(slash);
        const auto name = rootUrl.isLocalFile() ? slash : rootUrl.toDisplayString();
        m_components.push_back({ .name = name, .url = rootUrl });

        if (usedUrlPath.startsWith(slash)) {
            usedUrlPath = usedUrlPath.removeFirst();
        }
    }

    // Remove first slash as we have handled that just above
    const auto splitPath = usedUrlPath.split(slash);
    QString currentPath;

    for (const auto &pathComponent : splitPath) {
        currentPath += slash + pathComponent;
        QUrl currentUrl(url);
        currentUrl.setPath(currentPath);
        m_components.push_back({ .name = pathComponent, .url = currentUrl });
    }
    endResetModel();
}

int PathModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return m_components.size();
}

QVariant PathModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    const auto component = m_components.at(index.row());

    switch (role) {
    case Qt::DisplayRole:
        return component.name;
    case UrlRole:
        return component.url;
    }
    return {};
}

QHash<int, QByteArray> PathModel::roleNames() const
{
    auto baseRoleNames = QAbstractListModel::roleNames();
    baseRoleNames.insert({
        { UrlRole, "url" }
    });
    return baseRoleNames;
}

QUrl PathModel::followingPathComponent(const int row) const
{
    const auto componentCount = static_cast<int>(m_components.size());
    if (row < 0 || row >= componentCount - 1) {
        return {};
    }

    return m_components.at(row + 1).url;
}
