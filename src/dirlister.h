// SPDX-FileCopyrightText: 2020 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QObject>
#include <KDirLister>

class DirLister : public KDirLister
{
    Q_OBJECT

public:
    explicit DirLister(QObject *parent = nullptr);

public Q_SLOT:
    void handleError(KIO::Job *const job);

Q_SIGNALS:
    void errorOccured(const QString &message);
};
