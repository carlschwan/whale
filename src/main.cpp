/*
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QCommandLineParser>
#include <QStandardPaths>
#include <QIcon>

#include <KAboutData>
#include <KLocalizedString>
#include <KLocalizedContext>
#include <KLocalizedString>

using namespace Qt::StringLiterals;

int main(int argc, char **argv)
{

    QApplication app(argc, argv);
    app.setApplicationDisplayName(QStringLiteral("Whale"));
    app.setOrganizationDomain(QStringLiteral("kde.org"));
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("whale")));
    
    KLocalizedString::setApplicationDomain("whale");
    
    KAboutData aboutData(QStringLiteral("whale"),
                         xi18nc("@title", "<application>Whale</application>"),
                         QStringLiteral("0.1-dev"),
                         xi18nc("@title", "Whale is a file manager."),
                         KAboutLicense::GPL,
                         xi18nc("@info:credit", "(c) 2020 KDE Contributors"));

    aboutData.setOrganizationDomain(QByteArray("kde.org"));
    aboutData.setProductName(QByteArray("whale"));

    aboutData.addAuthor(xi18nc("@info:credit", "Carl Schwan"),
                        xi18nc("@info:credit", "Developer"),
                        QStringLiteral("carl@carlschwan.eu"));

    KAboutData::setApplicationData(aboutData);
    
    app.setApplicationVersion(aboutData.version());
    app.setApplicationName(aboutData.componentName());
    
    QCommandLineParser parser;

    aboutData.setupCommandLine(&parser);
    parser.process(app);
    aboutData.processCommandLine(&parser);

    // QML initialisation
    QQmlApplicationEngine engine;

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));

    // Set default path for file dialog
    // setFolder(QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::HomeLocation)));

    engine.loadFromModule(u"org.kde.whale"_s, u"Main"_s);

    return app.exec();
}
