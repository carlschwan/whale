// SPDX-FileCopyrightText: 2024 Claudio Cambra <claudio.cambra@kde.org>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QObject>
#include <qqmlregistration.h>
#include <QStandardPaths>
#include <QUrl>

#include <stack>

#include "pathmodel.h"

// [backward stack] < current url > [forward stack]

class NavigationStack : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

    Q_PROPERTY(QUrl currentUrl READ currentUrl NOTIFY currentUrlChanged)
    Q_PROPERTY(bool forwardsAvailable READ forwardsAvailable NOTIFY forwardsAvailableChanged)
    Q_PROPERTY(bool backwardsAvailable READ backwardsAvailable NOTIFY backwardsAvailableChanged)
    Q_PROPERTY(const PathModel *currentUrlModel READ currentUrlModel CONSTANT)

public:
    explicit NavigationStack();

    Q_INVOKABLE QUrl currentUrl() const;
    Q_INVOKABLE const PathModel *currentUrlModel() const;

    Q_INVOKABLE void pushUrl(const QUrl &url);
    Q_INVOKABLE void goBackwards();
    Q_INVOKABLE void goForwards();

    Q_INVOKABLE bool backwardsAvailable() const;
    Q_INVOKABLE bool forwardsAvailable() const;

Q_SIGNALS:
    void currentUrlChanged();

    void backwardsAvailableChanged();
    void forwardsAvailableChanged();

private:
    void setCurrentUrl(const QUrl &url);
    void updateCurrentUrlModel();

    QUrl m_currentUrl = QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::HomeLocation));
    std::stack<QUrl> m_backwardStack;
    std::stack<QUrl> m_forwardStack;

    PathModel m_currentUrlModel;
};

